module Authentication
  class Engine < ::Rails::Engine
    isolate_namespace Authentication

    require 'rails'
    require 'devise'
    require 'active_record/session_store'
    require 'omniauth-google-oauth2'

    # initializer "authentication", before: :load_config_initializers do |app|
    #   Rails.application.routes.append do
    #     mount Authentication::Engine, at: "/auth"
    #   end
    # end

  end
end
