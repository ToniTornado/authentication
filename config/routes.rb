Authentication::Engine.routes.draw do
  devise_for :users, path: '/', module: :devise,
  controllers: {
    omniauth_callbacks: 'authentication/omniauth_callbacks'
  },
  path_names: {
    sign_in: 'login',
    sign_out: 'logout'
    # password: 'secret',
    # confirmation: 'verification',
    # unlock: 'unblock',
    # registration: 'register',
    # sign_up: 'cmon_let_me_in'
  }
end
