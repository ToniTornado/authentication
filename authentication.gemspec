$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "authentication/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "authentication"
  s.version     = Authentication::VERSION
  s.authors     = ["Robin Wielpuetz"]
  s.email       = ["tonitornado@gmail.com"]
  s.homepage    = "https://github.com/tonitornado/authentication"
  s.summary     = "Basic Authentication Functionality."
  s.description = "Basic Authentication Functionality."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "activerecord-session_store"
  s.add_dependency "devise"
  s.add_dependency "omniauth-google-oauth2"

end
