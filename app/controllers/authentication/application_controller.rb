module Authentication
  class ApplicationController < ::ApplicationController

    def after_sign_in_path_for(user)
      main_app.root_path
    end

  end
end
