module Authentication
  class User < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :timeoutable, :registerable and :omniauthable
    devise :database_authenticatable, :lockable,
           :recoverable, :rememberable, :trackable, :validatable,
           :omniauthable, :omniauth_providers => [:google_oauth2]

    self.table_name = 'users'

    def self.from_omniauth(access_token)
      data = access_token.info
      user = ::User.find_by(email: data["email"])
      unless user
        user = ::User.create(
          first_name: data["first_name"],
          last_name: data["last_name"],
          email: data["email"],
          password: Devise.friendly_token[0,20]
        )
      end
      user
    end

    def is_admin?
      user_type == 'admin'
    end

  end
end
